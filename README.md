# Create a hetzner cloud vm with docker, docker-vm and nginx-proxy

## Requirements (local)

- terraform client

## Prerequisites

- run `cp vars.auto.tfvars.dist vars.auto.tfvars`
- Create api token in Hetzner Cloud. Add value to `vars.auto.tfvars` [HETZNER_API_TOKEN].

## Run Terraform

```
// Create ssh keys 
Run `./scripts/local/create-keys`

// Deploy server
tar czf ./proxy.tar.gz proxy/.
terraform init
terraform plan
echo 'yes' | terraform apply
// save ip addresses for later use
echo "NEW_IPV4=$(terraform output ipv4)" >> .env
echo "NEW_IPV6=$(terraform output ipv6)" >> .env
source .env
```

## Post Terraform steps
```
ssh -i keys/root_id_rsa root@${NEW_IPV4} '/bin/bash post_terraform_apply.sh'
```

## Debugging CloudInit
```
echo "NEW_IPV4=$(terraform output new_ipv4)" > .env
source .env
scp -i ./keys/root_id_rsa root@${NEW_IPV4}:/var/log/cloud-init.log .
```
