#!/usr/bin/env bash

# install docker-compose
curl -L https://github.com/docker/compose/releases/download/1.25.5/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

# start nginx-proxy
cd /home/proxy && /bin/bash ./initialize.sh

# ensure file access rights for deployment user
chown -R deployment:deployment /home/deployment
