variable "server_name" {}

variable "hcloud_token" {}

variable "vm_count" {}

variable "vm_os" {}

variable "vm_server_type" {}

provider "hcloud" {
  token = var.hcloud_token
}

resource "hcloud_ssh_key" "root-ssh" {
  name = "Root ssh key"
  public_key = file("./keys/root_id_rsa.pub")
}

data "template_file" "cloudinit" {
  template = file("./templates/cloudinit.yml")
  vars = {
    deployment_public_key = file("./keys/deployment_id_rsa.pub")
  }
}

resource "hcloud_server" "website-vm" {
  name = var.server_name
  server_type = var.vm_server_type
  image = var.vm_os
  location = "nbg1"
  ssh_keys = [hcloud_ssh_key.root-ssh.id]

  provisioner "file" {
    destination = "/root/post_terraform_apply.sh"
    source = "./scripts/remote/root_post_terraform_apply.sh"

    connection {
      host = self.ipv4_address
      type = "ssh"
      user = "root"
      private_key = file("keys/root_id_rsa")
    }
  }

  provisioner "file" {
    destination = "/home/proxy.tar.gz"
    source = "./proxy.tar.gz"

    connection {
      host = self.ipv4_address
      type = "ssh"
      user = "root"
      private_key = file("keys/root_id_rsa")
    }
  }

  provisioner "remote-exec" {
    inline = [
      "cd /home && tar xzf proxy.tar.gz"
    ]

    connection {
      host = self.ipv4_address
      type = "ssh"
      user = "root"
      private_key = file("keys/root_id_rsa")
    }
  }

  user_data = data.template_file.cloudinit.rendered
}

output "ipv4" {
  value = hcloud_server.website-vm.ipv4_address
}

output "ipv6" {
  value = hcloud_server.website-vm.ipv6_address
}
